package com.example.km_usb;

import android.app.Activity;
import android.app.PendingIntent;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.hardware.usb.UsbDevice;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import java.util.ArrayList;

public class BlueListActivity extends Activity {

    private static final String ACTION_USB_PERMISSION = "com.raycloud.kmprint";
    private final static String TAG = "UsbListActivity";

//    private ArrayList<BluetoothDevice> blueDevices = new ArrayList<>();

    private ArrayList<UsbDevice> usbDevices = new ArrayList<>();



    private ListView lv_blue;
    private UsbAdapter usbAdapter;
    private PendingIntent pendingIntent;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.blue_list_active);
//        initView();
        // 首先获取到意图对象
        Intent intent = getIntent();

        // 获取到传递过来的姓名
        usbDevices = intent.getParcelableArrayListExtra("blueDevices");
//                getStringExtra("seleType");

        lv_blue = findViewById(R.id.lv_blue);
        usbAdapter = new UsbAdapter();
        lv_blue.setAdapter(usbAdapter);


        pendingIntent = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_USB_PERMISSION), 0);
        lv_blue.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                UsbDevice usbDevice = usbDevices.get(position);

                Intent data = new Intent();
                data.putExtra("index",position);
                setResult(2,data);
                finish();
//                if (!usbManager.hasPermission(usbDevice)) {
//                    usbManager.requestPermission(usbDevice, pendingIntent);
//                }
//                connect(usbDevices.get(position));
            }
        });
    }

    class UsbAdapter extends BaseAdapter {

        LayoutInflater layoutInflater = LayoutInflater.from(BlueListActivity.this);

        @Override
        public int getCount() {
            return usbDevices.size();
        }

        @Override
        public UsbDevice getItem(int i) {
            return usbDevices.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View convertView, ViewGroup viewGroup) {
            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.blue_item_view, null);
            }
            TextView tvName = convertView.findViewById(R.id.tv_name);
            TextView tvAddress = convertView.findViewById(R.id.tv_address);
            UsbDevice usbDevice = getItem(i);
            tvName.setText(usbDevice.getDeviceName());
//            System.out.println("DeviceName："+bluetoothDevice.getDeviceName());
//            System.out.println("getSerialNumber()："+usbDevice.getSerialNumber());
//            System.out.println("getSerialNumber()："+usbDevice.getSerialNumber());
            tvAddress.setText(usbDevice.getSerialNumber());
            return convertView;
        }
    }




}
