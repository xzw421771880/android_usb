package com.example.km_usb;

import android.app.AlertDialog;
import android.app.PendingIntent;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class MainActivity extends AppCompatActivity {


    private static final String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";
    private UsbDevice currentDevice;
    String connectState = "1";


    private UsbManager usbManager;

    private UsbDeviceConnection usbDeviceConnection;

    private ArrayList<BluetoothDevice> bluetoothDevices = new ArrayList<>();

    private ArrayList<UsbDevice> usbDevices = new ArrayList<>();

    private UsbEndpoint usbEndpointIn;
    private UsbEndpoint usbEndpointOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


    }


    public void button1(View view) {

//        boolean ret=getPackageManager().hasSystemFeature("android.hardware.usb.host");

        // 判断当前手机是否支持usb.host功能
        boolean ret=getPackageManager().hasSystemFeature("android.hardware.usb.host");

        if (ret){

        }else {
            new AlertDialog.Builder(this).setTitle("提示").setMessage("不支持 USB host")
                    .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }})
                    .setNegativeButton("取消",null)
                    .show();
            return;
        }





//        Toast.makeText(this, "没有权限1", Toast.LENGTH_SHORT).show();


        usbManager = (UsbManager) getSystemService(Context.USB_SERVICE);


//        usbManager.getp
        HashMap<String, UsbDevice> map = usbManager.getDeviceList();

        usbDevices.clear();
        Iterator<UsbDevice> deviceIterator = map.values().iterator();
        while(deviceIterator.hasNext()){
            UsbDevice dev = deviceIterator.next();
//            System.out.println("deviceNumber"+dev.getSerialNumber());
            usbDevices.add(dev);
            //your code
        }

    }


    public void button2(View view) {


        System.out.println("2----");




        if (usbDevices.size() == 0){
            return;
        }

        Intent intent = new Intent(this,BlueListActivity.class);
//                    Intent intent = new Intent(this, BlueListActivity.class);
        System.out.println("usb列表个数"+usbDevices.size());
        intent.putExtra("blueDevices",usbDevices);
        startActivityForResult(intent,2);

    }


    public void button3(View view) {

        if (usbDeviceConnection == null){
            usbConnect();
        }

        byte[] sendbytes = "CLS\r\nSIZE 75mm,100mm\r\nPRINT 1,1\r\n".getBytes();

        int a =  usbDeviceConnection.bulkTransfer(usbEndpointOut, sendbytes, sendbytes.length, 500);
        System.out.println("3----"+a);

    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        System.out.println("返回-----");
        if (data != null){
            if (resultCode == 2){
                System.out.println("选中-----");

                final int index = data.getIntExtra("index",0);
                System.out.println("选中机器：" + index);
                currentDevice = usbDevices.get(index);
                System.out.println("选中机器：" + currentDevice.getDeviceName());



                usbConnect();

            }
        }

    }


    public void usbConnect(){
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, this.getIntent(), 0);

        if (!usbManager.hasPermission(currentDevice)) {

            usbManager.requestPermission(currentDevice, pendingIntent);
            return;

        }else {

        }
        usbDeviceConnection = usbManager.openDevice(currentDevice);
        if (usbDeviceConnection == null) {
            System.out.println("连接出错");
            Toast.makeText(this, "连接出错", Toast.LENGTH_SHORT).show();
            return;
        }
        int count = currentDevice.getInterfaceCount();
        for (int i = 0; i < count; i++) {
            UsbInterface usbInterface = currentDevice.getInterface(i);
            if (!usbDeviceConnection.claimInterface(usbInterface, true)) {
                usbDeviceConnection.close();
                System.out.println("独占失败");
                Toast.makeText(this, "独占失败", Toast.LENGTH_SHORT).show();
                return;
            }
            if (usbInterface.getInterfaceClass() == 7) {
                int endCount = usbInterface.getEndpointCount();
                System.out.println("endcount:" + endCount);
                for (int j = 0; j < endCount; j++) {
                    UsbEndpoint usbEndpoint = usbInterface.getEndpoint(j);
                    if (usbEndpoint.getDirection() == UsbConstants.USB_DIR_IN) {
                        usbEndpointIn = usbEndpoint;
                    }
                    if (usbEndpoint.getDirection() == UsbConstants.USB_DIR_OUT) {
                        usbEndpointOut = usbEndpoint;
                    }
                }
            }
        }

    }

}