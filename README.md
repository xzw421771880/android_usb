# Android_USB

#### 介绍
安卓通过usb连接硬件设备

#### 软件架构
Android Studio 4.1.2


#### 使用说明

1.  权限
在main添加
<uses-feature
        android:name="android.hardware.usb.host"
        android:required="true" />
2.  判断是否支持usb.host(比如chromeOS系统的电脑可以正常运行apk，apk可以用蓝牙但是不支持usb.host)
boolean ret=getPackageManager().hasSystemFeature("android.hardware.usb.host");
3.  获取usb外设
HashMap<String, UsbDevice> map = usbManager.getDeviceList();
4.  连接usb外设参考代码public void usbConnect()
5.  发送数据usbDeviceConnection.bulkTransfer

#### 参与贡献

1.  安卓usb使用文档
http://www.android-doc.com/guide/topics/connectivity/usb/host.html
2.  安卓开发者官网（了解chromeOS的支持）
https://developer.android.google.cn/topic/arc/manifest


#### 特技
更新中。。。。
